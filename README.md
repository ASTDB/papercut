# Papercut Print Job Processor

This application reads a set of print jobs from a CSV file and displays their individual and total costs to console. It's written in [Go](https://golang.org/) programming language.

# Configuration

A working Go installation is required to build and run the application. Instructions to download, install and configure Go for all major operating system platforms are available at https://golang.org/dl/ (for instance, Linux-specific information is available at https://golang.org/doc/install?download=go1.16.3.linux-amd64.tar.gz).

Once installed and the `go` command is available at a command line, download or clone the repository into a local directory. To build the application, run `go build Papercut.go` from repository directory. 

For flexibility, the application also reads in pricing information from a disk file in addition to print jobs. To execute the application, run `./Papercut sample.csv pricing.json` on a command line. 

To directly run the application (skipping the explicit build step), run `go run Papercut.go sample.csv pricing.json`. 

# Testing

Tests are included in the `Papercut_test.go` file within the repository. They can be invoked by running `go test` within the repository on a command line. 

# Code Design

This application uses a few modular steps to implement the specified functionality. It first reads pricing information from the pricing JSON file and print job information from the print jobs CSV file using separate functions. Pricing infomation is direcly parsed into a slice of pricing structs, and the CSV recordset is passed onto another function to process and transform into a slice of internal print job structs. It then iterates through the list of print job objects, calculating individual and total print costs and printing them out.

The CSV reader implementation checks for a future extra column on print job CSV file that could denote paper size. If not present, all jobs default to A4 size. The pricing JSON file has a paper size field which allows easy addition of pricing entries for extra paper sizes. 

# Future Improvements

A number of further improvements could be made to the application given time permits, summarized as below:

* More comprehensive unit and end-to-end tests.
* Constant-time search through pricing data structure through indexing by paper size and print type (utilizing a nested map)

# License

This software is provided free and as-is without any specific warranties for any purpose, commercial and/or non-commercial alike. 
