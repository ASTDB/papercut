/**
This program reads a set of print job records from a CSV input file and a set of print job prices from a JSON file, and calculates/outputs
print costs per job and as a total to the console. 
*/

package main

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	// read input filenames from commandline
	if len(os.Args) != 3 {
		log.Fatal("Usage: $> go run Papercut.go <printjobsfile> <pricingfile>")
	}

	printJobFileName := os.Args[1]
	pricingFileName := os.Args[2]

	// obtain print pricing information from price source (in this case, a JSON file)
	pricing, err := getPricing(pricingFileName)
	if err != nil {
		log.Fatal(err)
	}

	// extract print job records from CSV file
	csvRecords, errs := getCSVRecords(printJobFileName, 100)
	if errs != nil {
		log.Printf("Some errors occurred: %v\n", errs)

	}

	// obtain set of PrintJob objects from CSV print records
	printJobs, errs := getPrintJobs(csvRecords)
	if errs != nil {
		log.Printf("Some errors occurred: %v\n", errs)

	}

	// iterate through print jobs and calculate per job price and total price
	var totalPrice int
	for _, printJob := range printJobs {		
		thisJobPrice, err := calcJobPrice(printJob, pricing)
		if err != nil {
			log.Println(err)
			continue
		}

		log.Printf("%s / Cost: %s\n", printJob.ToString(), formatPrice(thisJobPrice))
		totalPrice += thisJobPrice
	}

	log.Printf("Total Cost: %s\n", formatPrice(totalPrice))

}

// getCSVRecords() accepts a CSV filename and a max. number of rows to read, and returns a slice of string slices representing
// CSV records, along with a slice of any errors encountered.
func getCSVRecords(fileName string, maxRecords int) ([][]string, []error) {
	var result [][]string
	var errorset []error

	file, err := os.Open(fileName)
	if err != nil {
		errorset = append(errorset, err)
		return result, errorset
	}
	defer file.Close()

	csvReader := csv.NewReader(file)

	// read CSV file per record upto the max number of rows to read or end-of-file
	rowCount := 0
	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		}

		if rowCount == 0 {
			// skip CSV header row
			rowCount++
			continue
		}

		result = append(result, record)

		if rowCount >= maxRecords {
			break
		}

		if err != nil {
			errorset = append(errorset, err)
		}

		rowCount++
	}

	return result, errorset
}

// getPrintJobs() accepts a set of CSV records representing print jobs and outputs a set of PrintJob objects,
// along with a slice of any errors encountered.
func getPrintJobs(printRecords [][]string) ([]*PrintJob, []error) {
	var result []*PrintJob
	var errorSet []error

	for _, record := range printRecords {
		var printJob *PrintJob
		var paperSize string
		var doubleSided bool
		var bwCount int
		var colorCount int

		// if a fourth CSV column denoting paper size is present, read it.
		// if not, default paper size to A4.
		if len(record) == 3 || len(record) == 4 {
			// get total page count
			totalPages, err := strconv.Atoi(strings.TrimSpace(record[0]))
			if err != nil {
				errorSet = append(errorSet, errors.New(fmt.Sprintf("Malformed print job - invalid total page count: %v\n", record)))
				continue
			}

			// get color/bw page counts
			colorCount, err = strconv.Atoi(strings.TrimSpace(record[1]))
			if err != nil {
				log.Println(err)
				log.Printf("Malformed print job - invalid color page count: %v\n", record)

				errorSet = append(errorSet, errors.New(fmt.Sprintf("Malformed print job - invalid color page count: %v\n", record)))
				continue
			}

			if totalPages < 0 || colorCount < 0 || totalPages < colorCount {
				errorSet = append(errorSet, errors.New(fmt.Sprintf("Malformed print job - color page count > total count: %v\n", record)))
				continue
			}

			bwCount = totalPages - colorCount

			printSide := strings.ToLower(strings.TrimSpace(record[2]))
			if printSide != "false" && printSide != "true" {
				errorSet = append(errorSet, errors.New(fmt.Sprintf("Malformed print job - invalid print side (non true/false column string): %v\n", record)))
				continue
			}

			doubleSided, err = strconv.ParseBool(printSide)
			if err != nil {
				errorSet = append(errorSet, errors.New(fmt.Sprintf("Malformed print job - invalid print side: %v\n", record)))
				continue
			}

			if len(record) == 3 {
				// paper sizes column isn't present in CSV, default to A4
				paperSize = "A4"
			} else if len(record) == 4 {
				//
				paperSize = strings.ToUpper(strings.TrimSpace(record[3]))

			}

		} else {
			errorSet = append(errorSet, errors.New(fmt.Sprintf("Malformed print job - incorrect field count: %v\n", record)))
			continue
		}

		printJob = &PrintJob{PaperSize: paperSize, DoubleSided: doubleSided, BWPages: bwCount, ColorPages: colorCount}
		result = append(result, printJob)
	}

	return result, errorSet
}

// getPricing() accepts file name and reads print job pricing info from JSON file, and returns a slice of job pricing
// objects along with any error encountered
func getPricing(priceFile string) ([]PrintPrice, error) {
	var printPrices []PrintPrice

	jsonPriceFile, err := os.Open(priceFile)
	if err != nil {
		return printPrices, err
	}

	defer jsonPriceFile.Close()

	priceByteVal, err := ioutil.ReadAll(jsonPriceFile)
	if err != nil {
		return printPrices, err
	}

	err = json.Unmarshal(priceByteVal, &printPrices)
	return printPrices, err
}

// calcJobPrice() accepts a PrintJob object and a print price listing, and returns the calculated price for
// that print job in cents object along with any error.
func calcJobPrice(printJob *PrintJob, pricing []PrintPrice) (int, error) {
	var jobPrice int
	var err error

	if printJob == nil {
		err = errors.New("Error calculating print job price - nil print job.")
		return jobPrice, err
	}

	if len(pricing) <= 0 {
		err = errors.New("Error calculating print job price - no prices available.")
		return jobPrice, err
	}

	for _, priceObj := range pricing {
		if priceObj.PaperSize == printJob.PaperSize && priceObj.DoubleSided == printJob.DoubleSided {
			jobPrice = (priceObj.BWPrice * printJob.BWPages) + (priceObj.ColorPrice * printJob.ColorPages)
			return jobPrice, err
		}
	}

	err = errors.New(fmt.Sprintf("Error calculating print job price - no price found for this print job (%s).", printJob.ToString()))
	return jobPrice, err
}

// formatPrice() accepts an int value representing a price in cents, and returns a human-readable formatted 
// string representing the dolla/cents price
func formatPrice(price int) string {
	var dollarComp int
	var centComp int
	var centStr string

	if price <= 0 {
		dollarComp = 0
		centComp = 0
		centStr = fmt.Sprintf("%d0", centComp)
	} else {
		dollarComp = price/100
		centComp = price % 100

		if centComp < 10 {
			centStr = fmt.Sprintf("%d0", centComp)
		} else {
			centStr = fmt.Sprintf("%d", centComp)
		}
	}

	return fmt.Sprintf("$%d.%s", dollarComp, centStr)
}

// struct type representing a print job
type PrintJob struct {
	PaperSize   string
	DoubleSided bool
	BWPages     int
	ColorPages  int
}

// helper method to print out a printjob to console/log
func (pj *PrintJob) ToString() string {
	var sb strings.Builder

	sb.WriteString(fmt.Sprintf("[PaperSize: %s, Doudle-Sided: %v, BWPages: %d, ColorPages: %d]", pj.PaperSize, pj.DoubleSided, pj.BWPages, pj.ColorPages))

	return sb.String()
}

type PrintPrice struct {
	PaperSize   string `json:"PaperSize"`
	DoubleSided bool   `json:"DoubleSided"`
	BWPrice     int    `json:"BWPrice"`
	ColorPrice  int    `json:"ColorPrice"`
}
