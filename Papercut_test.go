package main

import (
	"testing"
	"fmt"
	"strings"
	// "log"
)

var pricingFileName = "pricing.json"

// ------------ test getCSVRecords() ------------

// empty CSV file
func TestGetCSVRecordsEmptyFile(t *testing.T) {
	csvRecords, err := getCSVRecords("./test/TestInput01.csv", 100)
	if err != nil {
		t.Error(err)
		return
	}

	if len(csvRecords) != 0 {
		t.Error("Incorrect number of CSV records returned")
	}
}

// CSV file with single record
func TestGetCSVRecordsSingleRecord(t *testing.T) {
	csvRecords, err := getCSVRecords("./test/TestInput02.csv", 100)
	
	if err != nil {
		t.Error(err)
		return
	}

	if len(csvRecords) != 1 {
		t.Error("Incorrect number of CSV records returned")
		return
	}

	if strings.TrimSpace(csvRecords[0][0]) != "25" || strings.TrimSpace(csvRecords[0][1]) != "10" || strings.TrimSpace(csvRecords[0][2]) != "false" {
		t.Error("Incorrect CSV content read.")
	}
}

// CSV file with multiple records
func TestGetCSVRecordsMultipleRecords(t *testing.T) {
	csvRecords, err := getCSVRecords("./test/TestInput03.csv", 100)
	
	if err != nil {
		t.Error(err)
		return
	}

	if len(csvRecords) != 4 {
		t.Error("Incorrect number of CSV records returned")
		return
	}

	if strings.TrimSpace(csvRecords[0][0]) != "25" || strings.TrimSpace(csvRecords[0][1]) != "10" || strings.TrimSpace(csvRecords[0][2]) != "false" || strings.TrimSpace(csvRecords[1][0]) != "55" || strings.TrimSpace(csvRecords[1][1]) != "13" || strings.TrimSpace(csvRecords[1][2]) != "true" || strings.TrimSpace(csvRecords[2][0]) != "502" || strings.TrimSpace(csvRecords[2][1]) != "22" || strings.TrimSpace(csvRecords[2][2]) != "true" || strings.TrimSpace(csvRecords[3][0]) != "1" || strings.TrimSpace(csvRecords[3][1]) != "0" || strings.TrimSpace(csvRecords[3][2]) != "false" {
		t.Error("Incorrect CSV content read.")
	}
}

// test max rows to read on CSV file with multiple records
func TestGetCSVRecordsMaxRowsLimit(t *testing.T) {
	csvRecords, err := getCSVRecords("./test/TestInput03.csv", 2)
	
	if err != nil {
		t.Error(err)
		return
	}

	if len(csvRecords) != 2 {
		t.Error("Incorrect number of CSV records returned")
		return
	}
}

// ------------ test getPrintJobs() ------------

// zero CSV rows
func TestGetPrintJobsEmptyRecords(t *testing.T) {
	csvRecords, err := getCSVRecords("./test/TestInput01.csv", 100)
	if err != nil {
		t.Error(err)
		return
	}

	if len(csvRecords) != 0 {
		t.Error("Incorrect number of CSV records returned")
	}

	printJobs, err := getPrintJobs(csvRecords)
	if err != nil {
		t.Error(err)
		return
	}

	if len(printJobs) != 0 {
		t.Error("Incorrect number of PrintJob objects returned.")
	}
}

// single CSV record
func TestGetPrintJobsSingleRecord(t *testing.T) {
	csvRecords, err := getCSVRecords("./test/TestInput02.csv", 100)
	if err != nil {
		t.Error(err)
		return
	}

	if len(csvRecords) != 1 {
		t.Error("Incorrect number of CSV records returned")
	}

	printJobs, err := getPrintJobs(csvRecords)
	if err != nil {
		t.Error(err)
		return
	}

	if len(printJobs) != 1 {
		t.Error("Incorrect number of PrintJob objects returned.")
	}

	if printJobs[0].PaperSize != "A4" || printJobs[0].DoubleSided != false || printJobs[0].BWPages != 15 || printJobs[0].ColorPages != 10 {
		t.Error("Invalid PrintJob objects created.")
	}
}

// multiple CSV records
func TestGetPrintJobsMultipleRecords(t *testing.T) {
	csvRecords, err := getCSVRecords("./test/TestInput03.csv", 100)
	if err != nil {
		t.Error(err)
		return
	}

	if len(csvRecords) != 4 {
		t.Error("Incorrect number of CSV records returned")
	}

	printJobs, err := getPrintJobs(csvRecords)
	if err != nil {
		t.Error(err)
		return
	}

	if len(printJobs) != 4 {
		t.Error("Incorrect number of PrintJob objects returned.")
	}

	if printJobs[0].PaperSize != "A4" || printJobs[0].DoubleSided != false || printJobs[0].BWPages != 15 || printJobs[0].ColorPages != 10 || printJobs[1].PaperSize != "A4" || printJobs[1].DoubleSided != true || printJobs[1].BWPages != 42 || printJobs[1].ColorPages != 13 || printJobs[2].PaperSize != "A4" || printJobs[2].DoubleSided != true || printJobs[2].BWPages != 480 || printJobs[2].ColorPages != 22 || printJobs[3].PaperSize != "A4" || printJobs[3].DoubleSided != false || printJobs[3].BWPages != 1 || printJobs[3].ColorPages != 0 {
		t.Error("Invalid PrintJob objects created.")
	}
}

// invalid CSV record - length bigger than 4
func TestGetPrintJobsInvalidRecordLengthGT4(t *testing.T) {
	csvRecords := [][]string{{"25", "10", "FALSE", "A4", "##"}}

	_, err := getPrintJobs(csvRecords)
	if err == nil {
		t.Error(err)
		return
	}
}

// invalid CSV record - length bigger than 4
func TestGetPrintJobsInvalidRecordLengthLT3(t *testing.T) {
	csvRecords := [][]string{{"25", "10"}}

	_, err := getPrintJobs(csvRecords)
	if err == nil {
		t.Error(err)
		return
	}
}

// CSV record with future papersize field populated
func TestGetPrintJobsRecordWithPaperSize(t *testing.T) {
	csvRecords := [][]string{{"25", "10", "FALSE", "A3"}}

	printJobs, err := getPrintJobs(csvRecords)
	if err != nil {
		t.Error(err)
		return
	}

	if len(printJobs) <= 0 {
		t.Error("Zero-length print job set returned.")
		return
	}

	printJob := printJobs[0]
	if printJob == nil {
		t.Error("Nil print job returned.")
		return
	}

	if printJob.PaperSize != "A3" || printJob.DoubleSided != false || printJob.BWPages != 15 || printJob.ColorPages != 10 {
		t.Error("Invalid PrintJob objects created.")
		return
	} 
}

// CSV record with invalid page counts
func TestGetPrintJobsInvalidPageCounts(t *testing.T) {
	csvRecords := [][]string{{"10", "25", "FALSE"}}

	printJobs, err := getPrintJobs(csvRecords)
	if err == nil {
		t.Error(err)
		return
	}

	if len(printJobs) > 0 {
		t.Error("Non-zero length print job set returned.")
		return
	}
}

// CSV record with invalid print side
func TestGetPrintJobsInvalidPrintSide(t *testing.T) {
	csvRecords := [][]string{{"10", "25", "FOO"}}

	printJobs, err := getPrintJobs(csvRecords)
	if err == nil {
		t.Error(err)
		return
	}

	if len(printJobs) > 0 {
		t.Error("Non-zero length print job set returned.")
		return
	}
}

// ------------ test getPricing() ------------
func TestGetPricing(t *testing.T) {
	prices, err := getPricing(pricingFileName)
	if err != nil {
		t.Error(err)
	}

	if len(prices) != 2 {
		t.Error("Incorrect number of pricing objects returned.")
	}

	if prices[0].PaperSize != "A4" || prices[0].DoubleSided != false || prices[0].BWPrice != 15 || prices[0].ColorPrice != 25 || prices[1].PaperSize != "A4" || prices[1].DoubleSided != true || prices[1].BWPrice != 10 || prices[1].ColorPrice != 20 {
		t.Error("Incorrect pricing information read.")
	}
}

// ------------ test calcJobPrice() ------------

func TestCalcJobPrice01(t *testing.T) {
	pricing, err := getPricing(pricingFileName)
	if err != nil {
		t.Error(err)
	}

	printJob := &PrintJob{PaperSize: "A4", DoubleSided: false, BWPages: 15, ColorPages: 10}

	price, err := calcJobPrice(printJob, pricing)
	if err != nil {
		t.Error(err)
	}

	if price != 475 {
		t.Error(fmt.Sprintf("Error calculating job price for job %s (expected 475, returned %d)", printJob.ToString(), price))
	}
}

func TestCalcJobPrice02(t *testing.T) {
	pricing, err := getPricing(pricingFileName)
	if err != nil {
		t.Error(err)
	}

	printJob := &PrintJob{PaperSize: "A4", DoubleSided: true, BWPages: 42, ColorPages: 13}

	price, err := calcJobPrice(printJob, pricing)
	if err != nil {
		t.Error(err)
	}

	if price != 680 {
		t.Error(fmt.Sprintf("Error calculating job price for job %s (expected 680, returned %d)", printJob.ToString(), price))
	}
}

func TestCalcJobPrice03(t *testing.T) {
	pricing, err := getPricing(pricingFileName)
	if err != nil {
		t.Error(err)
	}

	printJob := &PrintJob{PaperSize: "A4", DoubleSided: true, BWPages: 480, ColorPages: 22}

	price, err := calcJobPrice(printJob, pricing)
	if err != nil {
		t.Error(err)
	}

	if price != 5240 {
		t.Error(fmt.Sprintf("Error calculating job price for job %s (expected 5240, returned %d)", printJob.ToString(), price))
	}
}

func TestCalcJobPrice04(t *testing.T) {
	pricing, err := getPricing(pricingFileName)
	if err != nil {
		t.Error(err)
	}

	printJob := &PrintJob{PaperSize: "A4", DoubleSided: false, BWPages: 1, ColorPages: 0}

	price, err := calcJobPrice(printJob, pricing)
	if err != nil {
		t.Error(err)
	}

	if price != 15 {
		t.Error(fmt.Sprintf("Error calculating job price for job %s (expected 15, returned %d)", printJob.ToString(), price))
	}
}

// ------------ test formatPrice() ------------

// test negative price
func TestFormatPriceNegative(t *testing.T) {
	price := formatPrice(-42)

	if price != "$0.00" {
		t.Error(fmt.Sprintf("Incorrect negative formatted price: %s (expected $0.00)", price))
	}
}

// test zero price
func TestFormatPriceZero(t *testing.T) {
	price := formatPrice(0)

	if price != "$0.00" {
		t.Error(fmt.Sprintf("Incorrect negative formatted price: %s (expected $0.00)", price))
	}
}

// test zero dollar comp
func TestFormatPricePositiveZeroDollarVal(t *testing.T) {
	price := formatPrice(10)

	if price != "$0.10" {
		t.Error(fmt.Sprintf("Incorrect negative formatted price: %s (expected $0.00)", price))
	}
}

// test zero cent comp
func TestFormatPricePositiveZeroCentVal(t *testing.T) {
	price := formatPrice(1000)

	if price != "$10.00" {
		t.Error(fmt.Sprintf("Incorrect negative formatted price: %s (expected $0.00)", price))
	}
}